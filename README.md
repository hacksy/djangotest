# ToDo Project

# Prerequisites
- [virtualenv](https://virtualenv.pypa.io/en/latest/)
- [postgresql](http://www.postgresql.org/)

# Before
- Install Python3.6

- If you use Linux install the follows packages
```bash
sudo apt-get install python3.6-dev, build-essential
```
- You have install postgresql and postgis
```bash
sudo apt-get install postgresql postgresql-contrib
```
- Then you configurate in /etc/postgresql/9.x/main/pg_hba.conf
```bash

      local   all             all                                     trust
      # IPv4 local connections:
      host    all             all             127.0.0.1/32            trust
      # IPv6 local connections:
      host    all             all             ::1/128                 trust
```

# Initialize the project
Create and activate a virtualenv:

```bash
virtualenv env
source env/bin/activate
```
Install dependencies:

```bash
pip install -r requirements/local.txt
```
Create the database:

```bash
sudo -u postgres createdb todo 
```
Initialize the git repository

```
git init
git remote add origin git@bitbucket.org:hacksy/djangotest.git
```

Migrate the database and create a superuser:
```bash
python todo/manage.py migrate
python todo/manage.py createsuperuser
```

Run the development server:
```bash
python todo/manage.py runserver
```