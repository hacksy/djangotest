# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.translation import pgettext_lazy, ugettext_lazy as _
from django.contrib.auth.models import User


class Todo(models.Model):
    """
    TODO Model
    Allows to create notes for an specific user if public
    """
    title   = models.CharField(_('Título'), max_length=255)
    content = models.TextField(_('Contenido'), blank=True)
    private = models.BooleanField(_('Nota privada'), default=False)
    user    = models.ForeignKey(User)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['title']
