from django.conf.urls import include, url
from .views import TodoView, TodoDetailsView, TodoReportView, TodoCreateView
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect

urlpatterns = [
	
    url(r'^home/$',login_required(TodoView.as_view()), name='id.views.todo.home'),
    url(r'^create/$', TodoCreateView.as_view(), name='id.views.todo.create'),
    url(r'^details/(?P<note_id>\d+)/$', TodoDetailsView.as_view(), name='id.views.todo.details'),
    url(r'^reports/$', TodoReportView.as_view(), name='id.views.todo.reports'),
]