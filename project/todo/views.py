# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.http import is_safe_url
from django.contrib.auth.forms import AuthenticationForm
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters
from django.views.generic import FormView, TemplateView
from django.db.models import Q
from django.shortcuts import get_object_or_404
from .models import Todo
from .forms import TodoForm

class TodoView(TemplateView):

    template_name = "todo/index.html"

    def get_context_data(self, **kwargs):
    	user = self.request.user
        context = super(TodoView, self).get_context_data(**kwargs)
        context["private_todos"] = Todo.objects.filter(private=True,user=user)
        context["public_todos"] = Todo.objects.filter(private=False)
        return context

class TodoCreateView(FormView):
    success_url = '/todo/create/'
    template_name = "todo/create.html"
    form_class = TodoForm
    def form_valid(self, form):
        self.object = form.save(commit = False)
        self.object.user = self.request.user
        self.object.save()
        return super(TodoCreateView, self).form_valid(form)


class TodoDetailsView(TemplateView):
	
    template_name = "todo/details.html"

    def get_context_data(self, **kwargs):
    	note_id = kwargs['note_id']
        user = self.request.user
        context = super(TodoDetailsView, self).get_context_data(**kwargs)
        #Get todo if public or if the current user is the owner
        if self.request.user.is_authenticated:
            context["todo"] = get_object_or_404(Todo, Q(private=False) | Q(user=user),id=note_id)
        else:
            context["todo"] = get_object_or_404(Todo, Q(private=False),id=note_id)
        return context

class TodoReportView(TemplateView):
	
    template_name = "todo/reports.html"

    def get_context_data(self, **kwargs):
    	user = self.request.user
        context = super(TodoReportView, self).get_context_data(**kwargs)
        context["all_notes_count"] = Todo.objects.all().count()
        context["my_private_notes_count"] = Todo.objects.filter(user=user,private=True).count()
        context["my_public_notes_count"] = Todo.objects.filter(user=user,private=False).count()
        context["public_notes_count"] = Todo.objects.filter(private=False).count()
        context["private_notes_count"] = Todo.objects.filter(private=True).count()
        return context