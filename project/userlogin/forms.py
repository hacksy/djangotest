from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth.models import User
from django.utils.translation import pgettext_lazy, ugettext_lazy as _


class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    last_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2', )

class UserProfileForm(UserChangeForm):
    fakename = forms.CharField(label=_("Nombre de usuario"),max_length=30,
     required=False, help_text='', disabled=True)
    first_name = forms.CharField(max_length=30, label=("Nombre"),
        required=False, help_text='')
    last_name = forms.CharField(max_length=30, label=("Apellido"),
     required=False, help_text='')
    email = forms.EmailField(max_length=254, help_text='',required=False)
    password1 = forms.CharField(label=_("Contrasena"),required=False,
        widget=forms.PasswordInput)
    password2 = forms.CharField(label=_("Confirmar Contrasena"),required=False,
        widget=forms.PasswordInput,
        help_text=_(""))
    password = None

    class Meta:
        model = User
        fields = ('fakename', 'first_name', 'last_name', 'email')
    