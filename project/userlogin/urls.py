from django.conf.urls import include, url
from .views import LogoutView, LoginView, ProfileView, RegisterView
from django.views.generic import RedirectView
from django.contrib.auth.decorators import login_required

urlpatterns = [
    url(r'^auth/login/$', LoginView.as_view(), name='id.views.auth'),
    url(r'^auth/logout/$', LogoutView.as_view(), name='id.views.logout'),
    url(r'^auth/profile/$', login_required(ProfileView.as_view()), name='id.views.profile'),
    url(r'^auth/register/$', RegisterView.as_view(), name='id.views.register'),
   	url(r'^$', RedirectView.as_view(pattern_name='id.views.auth', permanent=False)),
]