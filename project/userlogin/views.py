# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth import logout
from django.utils.http import is_safe_url
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import REDIRECT_FIELD_NAME, login as auth_login, logout as auth_logout
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters
from django.views.generic import FormView, RedirectView, TemplateView
from .forms import SignUpForm, UserProfileForm
from django.contrib.auth import authenticate, login
from django.contrib.auth.hashers import make_password

class LoginView(FormView):
    """
    Provides the ability to login as a user with a username and password
    """
    template_name = "userlogin/login.html"
    success_url = '/todo/home/'
    form_class = AuthenticationForm
    redirect_field_name = REDIRECT_FIELD_NAME

    @method_decorator(sensitive_post_parameters('password'))
    @method_decorator(csrf_protect)
    @method_decorator(never_cache)
    def dispatch(self, request, *args, **kwargs):
        # Sets a test cookie to make sure the user has cookies enabled
        request.session.set_test_cookie()

        return super(LoginView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        auth_login(self.request, form.get_user())

        # If the test cookie worked, go ahead and
        # delete it since its no longer needed
        if self.request.session.test_cookie_worked():
            self.request.session.delete_test_cookie()

        return super(LoginView, self).form_valid(form)

    def get_success_url(self):
        redirect_to = self.request.GET.get(self.redirect_field_name)
        if not is_safe_url(url=redirect_to, host=self.request.get_host()):
            redirect_to = self.success_url
        return redirect_to


class LogoutView(RedirectView):
    """
    Provides users the ability to logout
    """
    url = '/auth/login/'

    def get(self, request, *args, **kwargs):
        auth_logout(request)
        return super(LogoutView, self).get(request, *args, **kwargs)

class ProfileView(FormView):
    template_name = "userlogin/profile.html"
    success_url = '/auth/profile/'
    form_class = UserProfileForm
    redirect_field_name = REDIRECT_FIELD_NAME


    def form_valid(self, form):
        user = self.request.user

        first_name = form.cleaned_data.get('first_name')
        if first_name:
            user.first_name = first_name
        last_name = form.cleaned_data.get('last_name')
        if last_name:
            user.last_name = last_name
        email = form.cleaned_data.get('email')
        if email:
            user.email = email
        password1 = form.cleaned_data.get('password1')
        password2 = form.cleaned_data.get('password2')
        if len(password1) > 0:
            if password1 == password2:
                user.password = make_password(password1)
                user.save()
                logout(self.request)

        
        user.save()
        return super(ProfileView, self).form_valid(form)

    def get_success_url(self):
        redirect_to = self.request.GET.get(self.redirect_field_name)
        if not is_safe_url(url=redirect_to, host=self.request.get_host()):
            redirect_to = self.success_url
        return redirect_to

    def get_initial(self):
        initial = super(ProfileView, self).get_initial()
        initial['fakename'] = self.request.user.username
        initial['first_name'] = self.request.user.first_name
        initial['email'] = self.request.user.email
        initial['last_name'] = self.request.user.last_name
        return initial

class RegisterView(FormView):
    template_name = "userlogin/register.html"
    success_url = '/todo/home/'
    form_class = SignUpForm
    redirect_field_name = REDIRECT_FIELD_NAME
    
    def form_valid(self, form):
        form.save(commit = True)
        username = form.cleaned_data.get('username')
        raw_password = form.cleaned_data.get('password1')
        user = authenticate(username=username, password=raw_password)
        login(self.request, user)
        return super(RegisterView, self).form_valid(form)

    def get_success_url(self):
        redirect_to = self.request.GET.get(self.redirect_field_name)
        if not is_safe_url(url=redirect_to, host=self.request.get_host()):
            redirect_to = self.success_url
        return redirect_to